package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/sackufpi/beerschallenge/pkg/clients"
	"bitbucket.org/sackufpi/beerschallenge/pkg/db"
	"bitbucket.org/sackufpi/beerschallenge/pkg/services"
	"bitbucket.org/sackufpi/beerschallenge/pkg/transports"

	"github.com/go-kit/kit/log"
)

func main() {
	var (
		httpAddr = flag.String("http.addr", ":8080", "Address for HTTP (JSON) server")
	)
	flag.Parse()
	fmt.Println("Hello Beers Service")
	// Logging domain.
	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.With(logger, "ts", log.DefaultTimestampUTC)
		logger = log.With(logger, "caller", log.DefaultCaller)
	}

	psgSQLDbManager, err := db.InitializePgSQL()

	if err != nil {
		panic(err)
	}
	defer psgSQLDbManager.Conn.Close()

	dbAdapter := db.DatabaseAdapter{
		Db: psgSQLDbManager,
	}

	quoteClient := clients.InitializeMarketDataClient()

	srv := services.Initialize(dbAdapter, quoteClient)

	srv = services.NewLoggingMiddleware(logger, srv)

	requestCount, requestLatency, countResult := services.InitializeInstruments()
	srv = services.NewInstrumentingMiddleware(requestCount, requestLatency, countResult, srv)

	ctx := context.Background()
	r := transports.MakeHttpHandler(ctx, srv, logger)

	//log.Fatal(http.ListenAndServe(":8080", r))
	// Interrupt handler.
	errc := make(chan error)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errc <- fmt.Errorf("%s", <-c)
	}()

	// HTTP transport.
	go func() {
		logger.Log("transport", "HTTP", "addr", *httpAddr)
		errc <- http.ListenAndServe(*httpAddr, r)
	}()

	// Run!
	logger.Log("exit", <-errc)
}
