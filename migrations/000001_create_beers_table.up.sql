CREATE TABLE IF NOT EXISTS beers(
    id bigint PRIMARY KEY,
    name VARCHAR NOT NULL,
    brewery VARCHAR NOT NULL,
    currency VARCHAR NOT NULL,
    price NUMERIC NOT NULL
);