package models

import (
	"github.com/shopspring/decimal"
)

type BeerItem struct {
	ID       int
	Name     string
	Brewery  string
	Currency string
	Price    decimal.Decimal
}
