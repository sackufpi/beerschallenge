package endpoints

import (
	"context"

	"bitbucket.org/sackufpi/beerschallenge/pkg/services"

	"github.com/go-kit/kit/endpoint"
)

func MakeSearchBeersEndPoint(bs services.IBeers) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		desc, items, err := bs.SearchBeers(ctx)
		if err != nil {
			return services.SearchBeersResponse{
				Description: desc,
				Items:       items,
				Err:         err.Error()}, err
		}
		return services.SearchBeersResponse{
			Description: desc,
			Items:       items,
			Err:         ""}, nil
	}
}

func MakeAddBeersEndPoint(bs services.IBeers) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(services.AddBeersRequest)
		desc, err := bs.AddBeers(ctx, req.ID, req.Name, req.Brewery, req.Currency, req.Price)

		if err != nil {
			return services.AddBeersResponse{
				Description: desc,
				Err:         err.Error()}, err
		}
		return services.AddBeersResponse{
			Description: desc,
			Err:         ""}, nil
	}
}

func MakeSearchBeerByIdEndPoint(bs services.IBeers) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(services.SearchBeerByIdRequest)
		desc, item, err := bs.SearchBeerById(ctx, req.BeerID)
		if err != nil {
			return services.SearchBeerByIdResponse{
				Description: desc,
				Item:        item,
				Err:         err.Error()}, err
		}
		return services.SearchBeerByIdResponse{
			Description: desc,
			Item:        item,
			Err:         ""}, nil
	}
}

func MakeBoxBeerPriceByIdEndPoint(bs services.IBeers) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(services.BoxBeerPriceByIdRequest)
		desc, price, err := bs.BoxBeerPriceById(ctx, req.BeerID, req.Quantity, req.Currency)
		if err != nil {
			return services.BoxBeerPriceByIdResponse{
				Description: desc,
				Price:       price,
				Err:         err.Error()}, err
		}
		return services.BoxBeerPriceByIdResponse{
			Description: desc,
			Price:       price,
			Err:         ""}, nil
	}
}
