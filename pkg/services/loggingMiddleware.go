package services

import (
	"context"
	"fmt"
	"time"

	"bitbucket.org/sackufpi/beerschallenge/pkg/models"

	"github.com/go-kit/kit/log"
	"github.com/shopspring/decimal"
)

type loggingMiddleware struct {
	logger log.Logger
	bs     IBeers
}

func (mw loggingMiddleware) SearchBeers(ctx context.Context) (desc string, res []models.BeerItem, err error) {

	defer func(begin time.Time) {
		mw.logger.Log(
			"method", "SearchBeers",
			"input", "",
			"output", fmt.Sprintf("%+v", res),
			"err", err,
			"took", time.Since(begin),
		)
	}(time.Now())

	desc, res, err = mw.bs.SearchBeers(ctx)

	return
}

func (mw loggingMiddleware) AddBeers(ctx context.Context, id int, name, brewery, currency string, price decimal.Decimal) (desc string, err error) {
	defer func(begin time.Time) {
		mw.logger.Log(
			"method", "AddBeer",
			"input", fmt.Sprintf("ID: %d, Name: %s, Brewery: %s, Currency: %s, Price: %s", id, name, brewery, currency, price.String()),
			"output", desc,
			"err", err,
			"took", time.Since(begin),
		)
	}(time.Now())

	desc, err = mw.bs.AddBeers(ctx, id, name, brewery, currency, price)
	return
}

func (mw loggingMiddleware) SearchBeerById(ctx context.Context, beerId int) (desc string, item models.BeerItem, err error) {
	defer func(begin time.Time) {
		mw.logger.Log(
			"method", "SearchBeerById",
			"input", fmt.Sprintf("ID: %d", beerId),
			"output", fmt.Sprintf("%+v", item),
			"err", err,
			"took", time.Since(begin),
		)
	}(time.Now())

	desc, item, err = mw.bs.SearchBeerById(ctx, beerId)
	return
}

func (mw loggingMiddleware) BoxBeerPriceById(ctx context.Context, beerId int, quantity int32, currency string) (desc string, price decimal.Decimal, err error) {
	defer func(begin time.Time) {
		mw.logger.Log(
			"method", "BoxBeerPriceById",
			"input", fmt.Sprintf("ID: %d, Quantity: %d, Currency: %s", beerId, quantity, currency),
			"output", price.String(),
			"err", err,
			"took", time.Since(begin),
		)
	}(time.Now())

	desc, price, err = mw.bs.BoxBeerPriceById(ctx, beerId, quantity, currency)
	return
}

func NewLoggingMiddleware(logger log.Logger, bs IBeers) IBeers {
	return loggingMiddleware{logger, bs}
}
