package services

import (
	"context"

	"bitbucket.org/sackufpi/beerschallenge/pkg/clients"
	"bitbucket.org/sackufpi/beerschallenge/pkg/db"
	"bitbucket.org/sackufpi/beerschallenge/pkg/models"
	"github.com/shopspring/decimal"
)

type IBeers interface {
	AddBeers(context.Context, int, string, string, string, decimal.Decimal) (string, error)
	SearchBeers(context.Context) (string, []models.BeerItem, error)
	SearchBeerById(context.Context, int) (string, models.BeerItem, error)
	BoxBeerPriceById(context.Context, int, int32, string) (string, decimal.Decimal, error)
}

type beersService struct {
	DbConnection db.IDatabase
	QuoteClient  clients.IExchangeRate
}

func (bs beersService) SearchBeers(_ context.Context) (string, []models.BeerItem, error) {

	items, err := bs.DbConnection.SearchBeers()
	if err != nil {
		return "Error searching beers", items, err
	}

	return "Successful operation.", items, nil
}

func (bs beersService) AddBeers(ctx context.Context, id int, name, brewery, currency string, price decimal.Decimal) (string, error) {
	newBeer := models.BeerItem{id, name, brewery, currency, price}
	err := bs.DbConnection.AddBeers(newBeer)

	if err != nil {
		return "Unable to insert a new beer", err
	}

	return "A new beer has been created.", nil
}

func (bs beersService) SearchBeerById(ctx context.Context, beerId int) (string, models.BeerItem, error) {
	beer, err := bs.DbConnection.SearchBeerById(beerId)

	if err != nil {
		return "The operation has failed", models.BeerItem{}, err
	}

	return "Successful operation.", beer, nil
}

func (bs beersService) BoxBeerPriceById(ctx context.Context, beerId int, quantity int32, currency string) (string, decimal.Decimal, error) {

	beer, err := bs.DbConnection.SearchBeerById(beerId)

	if err != nil {
		return "The Operation has failed", decimal.Decimal{}, err
	}

	decimalQuatity := decimal.NewFromInt32(quantity)

	if beer.Currency != currency {
		quoteCurrency, errQuote := bs.QuoteClient.GetQuotes(beer.Currency, currency)

		if errQuote != nil {
			return "The Operation has failed", decimal.Decimal{}, errQuote
		}
		subTotal := beer.Price.Mul(quoteCurrency)
		total := subTotal.Mul(decimalQuatity)

		return "Successful operation.", total, nil
	}

	return "Successful operation.", beer.Price.Mul(decimalQuatity), nil
}

func Initialize(dbConnection db.IDatabase, quoteClient clients.IExchangeRate) IBeers {
	return beersService{
		DbConnection: dbConnection,
		QuoteClient:  quoteClient,
	}
}
