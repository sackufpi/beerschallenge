package services

import (
	"context"
	"fmt"
	"time"

	"bitbucket.org/sackufpi/beerschallenge/pkg/models"

	"github.com/go-kit/kit/metrics"
	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	"github.com/shopspring/decimal"
)

func InitializeInstruments() (metrics.Counter, metrics.Histogram, metrics.Histogram) {
	fieldKeys := []string{"method", "error"}
	requestCount := kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
		Namespace: "my_group",
		Subsystem: "beers",
		Name:      "request_count",
		Help:      "Number of requests received.",
	}, fieldKeys)
	requestLatency := kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
		Namespace: "my_group",
		Subsystem: "beers",
		Name:      "request_latency_microseconds",
		Help:      "Total duration of requests in microseconds.",
	}, fieldKeys)
	countResult := kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
		Namespace: "my_group",
		Subsystem: "beers",
		Name:      "count_result",
		Help:      "The result of each count method.",
	}, []string{}) // no fields here

	return requestCount, requestLatency, countResult
}

type instrumentingMiddleware struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	countResult    metrics.Histogram
	bs             IBeers
}

func (mw instrumentingMiddleware) SearchBeers(ctx context.Context) (desc string, res []models.BeerItem, err error) {
	defer func(begin time.Time) {
		lvs := []string{"method", "SearchBeers", "error", fmt.Sprint(err != nil)}
		mw.requestCount.With(lvs...).Add(1)
		mw.requestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	desc, res, err = mw.bs.SearchBeers(ctx)
	return
}

func (mw instrumentingMiddleware) AddBeers(ctx context.Context, id int, name, brewery, currency string, price decimal.Decimal) (desc string, err error) {
	defer func(begin time.Time) {
		lvs := []string{"method", "AddBeers", "error", fmt.Sprint(err != nil)}
		mw.requestCount.With(lvs...).Add(1)
		mw.requestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	desc, err = mw.bs.AddBeers(ctx, id, name, brewery, currency, price)
	return
}

func (mw instrumentingMiddleware) SearchBeerById(ctx context.Context, beerID int) (desc string, item models.BeerItem, err error) {
	defer func(begin time.Time) {
		lvs := []string{"method", "SearchBeerById", "error", fmt.Sprint(err != nil)}
		mw.requestCount.With(lvs...).Add(1)
		mw.requestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	desc, item, err = mw.bs.SearchBeerById(ctx, beerID)
	return
}

func (mw instrumentingMiddleware) BoxBeerPriceById(ctx context.Context, beerID int, quantity int32, currency string) (desc string, price decimal.Decimal, err error) {
	defer func(begin time.Time) {
		lvs := []string{"method", "BoxBeerPriceById", "error", fmt.Sprint(err != nil)}
		mw.requestCount.With(lvs...).Add(1)
		mw.requestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	desc, price, err = mw.bs.BoxBeerPriceById(ctx, beerID, quantity, currency)
	return
}

func NewInstrumentingMiddleware(requestCount metrics.Counter,
	requestLatency metrics.Histogram,
	countResult metrics.Histogram,
	bs IBeers) IBeers {

	return instrumentingMiddleware{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		countResult:    countResult,
		bs:             bs}
}
