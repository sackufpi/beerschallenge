package services

import (
	"bitbucket.org/sackufpi/beerschallenge/pkg/models"
	"github.com/shopspring/decimal"
)

type SearchBeersRequest struct{}

type SearchBeersResponse struct {
	Description string            `json:"description"`
	Items       []models.BeerItem `json:"beers"`
	Err         string            `json:"err,omitempty"`
}

type AddBeersRequest struct {
	ID       int
	Name     string
	Brewery  string
	Currency string
	Price    decimal.Decimal
}

type AddBeersResponse struct {
	Description string `json:"description"`
	Err         string `json:"err,omitempty"`
}

type SearchBeerByIdRequest struct {
	BeerID int
}

type SearchBeerByIdResponse struct {
	Description string          `json:"description"`
	Item        models.BeerItem `json:"beers"`
	Err         string          `json:"err,omitempty"`
}

type BoxBeerPriceByIdRequest struct {
	BeerID   int
	Quantity int32
	Currency string
}

type BoxBeerPriceByIdResponse struct {
	Description string          `json:"description"`
	Price       decimal.Decimal `json:"Price Total"`
	Err         string          `json:"err,omitempty"`
}
