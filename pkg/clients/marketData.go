package clients

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"reflect"

	"github.com/shopspring/decimal"
)

type Data struct {
	Endpoint       string                   `json:'endpoint'`
	Quotes         []map[string]interface{} `json:'quotes'`
	Requested_time string                   `json:'requested_time'`
	Timestamp      int32                    `json:'timestamp'`
}

func InitializeMarketDataClient() MarketDataClient {
	baseURL, apiKey :=
		os.Getenv("MARKETDATA_BASE_URL"),
		os.Getenv("MARKETDATA_API_KEY")

	quoteClient := MarketDataClient{
		ApiKey:  apiKey,
		BaseUrl: baseURL,
	}

	return quoteClient
}

type MarketDataClient struct {
	ApiKey  string
	BaseUrl string
}

func (mktClient MarketDataClient) GetQuotes(fromCurrency, toCurrency string) (decimal.Decimal, error) {
	currencies := fromCurrency + toCurrency
	url := mktClient.BaseUrl + "?currency=" + currencies + "&api_key=" + mktClient.ApiKey
	emptyDecimal := decimal.Decimal{}

	resp, err := http.Get(url)
	if err != nil {
		return emptyDecimal, err
	}

	body, errRead := ioutil.ReadAll(resp.Body)
	if errRead != nil {
		return emptyDecimal, errRead
	}

	dataObj := Data{}

	errDec := json.Unmarshal(body, &dataObj)
	if errDec != nil {
		return emptyDecimal, errDec
	}

	fmt.Println("endpoint", dataObj.Endpoint, "requested time", dataObj.Requested_time, "timestamp", dataObj.Timestamp)

	for _, value := range dataObj.Quotes {

		v, err := getFloat(value["ask"])
		if err != nil {
			return emptyDecimal, err
		}
		quoteValue := decimal.NewFromFloat(v)
		return quoteValue, nil
	}
	return emptyDecimal, errors.New("Error getting Quote")
}

var floatType = reflect.TypeOf(float64(0))

func getFloat(unk interface{}) (float64, error) {
	v := reflect.ValueOf(unk)
	v = reflect.Indirect(v)
	if !v.Type().ConvertibleTo(floatType) {
		return 0, fmt.Errorf("cannot convert %v to float64", v.Type())
	}
	fv := v.Convert(floatType)
	return fv.Float(), nil
}
