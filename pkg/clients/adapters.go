package clients

import "github.com/shopspring/decimal"

type MarketDataClientAdapter struct {
	Client IExchangeRate
}

func (mktClientAdapter MarketDataClientAdapter) GetQuotes(fromCurrency, toCurrency string) (decimal.Decimal, error) {
	return mktClientAdapter.Client.GetQuotes(fromCurrency, toCurrency)
}
