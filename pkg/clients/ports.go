package clients

import (
	"github.com/shopspring/decimal"
)

type IExchangeRate interface {
	GetQuotes(string, string) (decimal.Decimal, error)
}
