package transports

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"bitbucket.org/sackufpi/beerschallenge/pkg/endpoints"
	"bitbucket.org/sackufpi/beerschallenge/pkg/services"

	"github.com/go-kit/kit/log"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func MakeHttpHandler(ctx context.Context, bs services.IBeers, logger log.Logger) http.Handler {

	searchBeersEndpoint := endpoints.MakeSearchBeersEndPoint(bs)
	searchBeersEndpoint = endpoints.LoggingMiddleware(log.With(logger, "method", "searchBeersEndpoint"))(searchBeersEndpoint)

	addBeersEndpoint := endpoints.MakeAddBeersEndPoint(bs)
	addBeersEndpoint = endpoints.LoggingMiddleware(log.With(logger, "method", "addBeersEndpoint"))(addBeersEndpoint)

	searchBeerByIdEndpoint := endpoints.MakeSearchBeerByIdEndPoint(bs)
	searchBeerByIdEndpoint = endpoints.LoggingMiddleware(log.With(logger, "method", "SearchBeerByIdEndpoint"))(searchBeerByIdEndpoint)

	boxBeerPriceByIdEndpoint := endpoints.MakeBoxBeerPriceByIdEndPoint(bs)
	boxBeerPriceByIdEndpoint = endpoints.LoggingMiddleware(log.With(logger, "method", "BoxBeerPriceByIdEndpoint"))(boxBeerPriceByIdEndpoint)

	r := mux.NewRouter()
	searchBeersHandler := httptransport.NewServer(
		searchBeersEndpoint,
		httptransport.NopRequestDecoder,
		encodeResponse,
	)

	addBeersHandler := httptransport.NewServer(
		addBeersEndpoint,
		decodeAddBeersRequest,
		encodeResponse,
	)

	searchBeerByIdHandler := httptransport.NewServer(
		searchBeerByIdEndpoint,
		decodeSearchBeerByIdRequest,
		encodeResponse,
	)

	boxBeerPriceByIdHandler := httptransport.NewServer(
		boxBeerPriceByIdEndpoint,
		decodeBoxBeerPriceByIdRequest,
		encodeResponse,
	)

	r.Methods("GET").Path("/beers").Handler(searchBeersHandler)
	r.Methods("GET").Path("/beers/{beerID}").Handler(searchBeerByIdHandler)
	r.Methods("GET").Path("/beers/{beerID}/boxprice").Handler(boxBeerPriceByIdHandler)
	r.Methods("POST").Path("/beers").Handler(addBeersHandler)

	r.Methods("GET").Path("/metrics").Handler(promhttp.Handler())

	return r
}

func decodeAddBeersRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request services.AddBeersRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func decodeSearchBeerByIdRequest(_ context.Context, r *http.Request) (interface{}, error) {
	params := mux.Vars(r)

	beerID, err := strconv.Atoi(params["beerID"])
	if err != nil {
		return nil, err
	}
	request := services.SearchBeerByIdRequest{BeerID: beerID}

	return request, nil
}

func decodeBoxBeerPriceByIdRequest(_ context.Context, r *http.Request) (interface{}, error) {
	params := mux.Vars(r)

	var quantity int32
	currency := r.URL.Query().Get("currency")
	if currency == "" {
		return nil, errors.New("The Currency query parameter must have a value")
	}

	q := r.URL.Query().Get("quantity")
	if q != "" {
		i, errParse := strconv.ParseInt(q, 10, 32)
		if errParse != nil {
			return nil, errParse
		}
		quantity = int32(i)
	} else {
		quantity = 6
	}

	beerID, err := strconv.Atoi(params["beerID"])
	if err != nil {
		return nil, err
	}
	request := services.BoxBeerPriceByIdRequest{BeerID: beerID, Quantity: quantity, Currency: currency}

	return request, nil
}

// errorer is implemented by all concrete response types that may contain
// errors. It allows us to change the HTTP response code without needing to
// trigger an endpoint (transport-level) error.
type errorer interface {
	error() error
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		// Not a Go kit transport error, but a business-logic error.
		// Provide those as HTTP errors.
		encodeError(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	if err == nil {
		panic("encodeError with nil error")
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusInternalServerError)
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}
