package db

import "bitbucket.org/sackufpi/beerschallenge/pkg/models"

type DatabaseAdapter struct {
	Db IDatabase
}

func (dbAdapter DatabaseAdapter) AddBeers(item models.BeerItem) error {
	return dbAdapter.Db.AddBeers(item)
}

func (dbAdapter DatabaseAdapter) SearchBeers() ([]models.BeerItem, error) {
	return dbAdapter.Db.SearchBeers()
}

func (dbAdapter DatabaseAdapter) SearchBeerById(beerID int) (models.BeerItem, error) {
	return dbAdapter.Db.SearchBeerById(beerID)
}
