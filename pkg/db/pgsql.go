package db

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"strconv"

	"bitbucket.org/sackufpi/beerschallenge/pkg/models"
	_ "github.com/lib/pq"
	"github.com/shopspring/decimal"
)

var ErrNoMatch = fmt.Errorf("no matching record")

type PgSqlDatabaseManager struct {
	Conn *sql.DB
}

func InitializePgSQL() (PgSqlDatabaseManager, error) {
	username, password, database, hostname :=
		os.Getenv("POSTGRES_USER"),
		os.Getenv("POSTGRES_PASSWORD"),
		os.Getenv("POSTGRES_DB"),
		os.Getenv("POSTGRES_HOST")

	port, err := strconv.Atoi(os.Getenv("POSTGRES_PORT"))

	if err != nil {
		return PgSqlDatabaseManager{}, err
	}

	dsn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		hostname, port, username, password, database)
	conn, err := sql.Open("postgres", dsn)
	if err != nil {
		return PgSqlDatabaseManager{}, err
	}

	err = conn.Ping()
	if err != nil {
		return PgSqlDatabaseManager{}, err
	}
	log.Println("Database connection established")
	db := PgSqlDatabaseManager{
		Conn: conn,
	}
	return db, nil
}

func (psqlDb PgSqlDatabaseManager) AddBeers(item models.BeerItem) error {
	var id int
	var price decimal.Decimal
	var name, brewery, currency string

	query := `INSERT INTO beers (id, name, brewery, currency, price) VALUES ($1, $2, $3, $4, $5) RETURNING id, name, brewery, currency, price`
	err := psqlDb.Conn.QueryRow(query, item.ID, item.Name, item.Brewery, item.Currency, item.Price).Scan(&id, &name, &brewery, &currency, &price)
	if err != nil {
		return err
	}
	item.ID = id
	item.Name = name
	item.Brewery = brewery
	item.Currency = currency
	item.Price = price

	return nil
}

func (psqlDb PgSqlDatabaseManager) SearchBeers() ([]models.BeerItem, error) {
	var allItems []models.BeerItem
	rows, err := psqlDb.Conn.Query("SELECT * FROM beers ORDER BY ID DESC")
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var item models.BeerItem
		err := rows.Scan(&item.ID, &item.Name, &item.Brewery, &item.Currency, &item.Price)
		if err != nil {
			return nil, err
		}
		allItems = append(allItems, item)
	}
	return allItems, nil
}

func (psqlDb PgSqlDatabaseManager) SearchBeerById(beerID int) (models.BeerItem, error) {
	item := models.BeerItem{}
	query := `SELECT * FROM beers WHERE id = $1;`
	row := psqlDb.Conn.QueryRow(query, beerID)

	switch err := row.Scan(&item.ID, &item.Name, &item.Brewery, &item.Currency, &item.Price); err {
	case sql.ErrNoRows:
		return item, ErrNoMatch
	default:
		return item, err
	}
}
