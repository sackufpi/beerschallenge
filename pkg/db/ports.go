package db

import "bitbucket.org/sackufpi/beerschallenge/pkg/models"

type IDatabase interface {
	AddBeers(models.BeerItem) error
	SearchBeers() ([]models.BeerItem, error)
	SearchBeerById(int) (models.BeerItem, error)
}
