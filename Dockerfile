# Start from the latest golang base image
FROM golang:latest as builder

ENV GO111MODULE=on

COPY go.mod go.sum /go/src/bitbucket.org/sackufpi/beerschallenge/

WORKDIR /go/src/bitbucket.org/sackufpi/beerschallenge/

COPY . .

RUN go mod download

#RUN go build -o /go/bin/beerschallenge cmd/beers/main.go
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o build/beerschallenge bitbucket.org/sackufpi/beerschallenge/cmd/beers


FROM alpine
RUN apk add --no-cache ca-certificates && update-ca-certificates
COPY --from=builder /go/src/bitbucket.org/sackufpi/beerschallenge/build/beerschallenge /user/bin/beerschallenge
EXPOSE 8080 8080
ENTRYPOINT ["/user/bin/beerschallenge"]